;(function($, window, document, undefined){
	
	var $timeZonePicker, $monthPicker, $yearPicker;
	
	$(function(){
		init();
		initTimeZonePicker();
		initMonthPicker();
		initYearPicker();
	});
	
	function init(){
		$timeZonePicker = $("#timezone-picker");
		$monthPicker = $("#month-picker");
		$yearPicker = $("#year-picker");
	}
	
	function initTimeZonePicker(){
		$timeZonePicker.on("change", function(){
			var $this = $(this);
			window.location = '/Task1.php?time='+$this.val()+'&month='+$monthPicker.val() + '&year=' + $yearPicker.val();
		});
	}
	
	function initMonthPicker(){
		$monthPicker.on("change", function(){
			var $this = $(this);
			window.location = '/Task1.php?time='+$timeZonePicker.val() + '&month=' + $this.val() + '&year=' + $yearPicker.val();
		});
	}
	
	function initYearPicker(){
		$yearPicker.on("change", function(){
			var $this = $(this);
			window.location = '/Task1.php?time='+$timeZonePicker.val() + '&month=' + $monthPicker.val() + '&year=' + $this.val();
		});
	}
	
})(jQuery, window, document);