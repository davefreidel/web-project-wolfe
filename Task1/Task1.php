<?php

include("server/functions.php");


?><!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<title>Calendar</title>
	<link href="/styles/main.css" rel="stylesheet" />
	<script type="text/javascript" src="/scripts/jquery.js"></script>
	<script type="text/javascript" src="/scripts/main.js"></script>
</head>

<body>



	<div class="page">
	
		<select id="timezone-picker">
			<option <?php echo timeZonePickerOption('America/New_York'); ?>>New York</option>
			<option <?php echo timeZonePickerOption('America/Chicago'); ?>>Chicago</option>
			<option <?php echo timeZonePickerOption('America/Denver'); ?>>Denver</option>
			<option <?php echo timeZonePickerOption('America/Los_Angeles'); ?>>Los Angeles</option>
			<option <?php echo timeZonePickerOption('America/Anchorage'); ?>>Anchorage</option>
		</select>
	
		<select id="month-picker">
			<option <?php echo monthPickerOption(1); ?> value="1">January</option>
			<option <?php echo monthPickerOption(2); ?> value="2">February</option>
			<option <?php echo monthPickerOption(3); ?> value="3">March</option>
			<option <?php echo monthPickerOption(4); ?> value="4">April</option>
			<option <?php echo monthPickerOption(5); ?> value="5">May</option>
			<option <?php echo monthPickerOption(6); ?> value="6">June</option>
			<option <?php echo monthPickerOption(7); ?> value="7">July</option>
			<option <?php echo monthPickerOption(8); ?> value="8">August</option>
			<option <?php echo monthPickerOption(9); ?> value="9">September</option>
			<option <?php echo monthPickerOption(10); ?> value="10">October</option>
			<option <?php echo monthPickerOption(11); ?> value="11">November</option>
			<option <?php echo monthPickerOption(12); ?> value="12">December</option>
		</select>
	
		<select id="year-picker">
			<option <?php echo yearPickerOption(2017); ?> value="2017">2017</option>
			<option <?php echo yearPickerOption(2018); ?> value="2018">2018</option>
			<option <?php echo yearPickerOption(2019); ?> value="2019">2019</option>
			<option <?php echo yearPickerOption(2020); ?> value="2020">2020</option>
		</select>
		
		<div id="calendar-title"><?php echo getCalendarTitle(); ?></div>
	
		<table id="calendar">
		
			<thead>
				<tr>
					<th>Sunday</th>
					<th>Monday</th>	
					<th>Tuesday</th>
					<th>Wednesday</th>	
					<th>Thursday</th>
					<th>Friday</th>	
					<th>Saturday</th>
				</tr>	
			</thead>
			<tbody><?php
			
				echo createCalendar();
			
				?>
			</tbody>
		</table>

	</div>

</body>
</html>
