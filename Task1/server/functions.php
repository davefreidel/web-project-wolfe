<?php

// Some $_GET variables and fallbacks
$my_month = isset($_GET['month']) ? $_GET['month'] : date("n");
$my_year = isset($_GET['year']) ? $_GET['year'] : date("Y");
$my_timezone = isset($_GET['time']) ? $_GET['time'] : 'America/New_York';

date_default_timezone_set($my_timezone);


/* return string
 * Checks if $_GET timezone is set and selects appropriate option
 */
function timeZonePickerOption($timezone){
	global $my_timezone;
	return ($my_timezone == $timezone) ? ' selected value="'.$timezone.'" ' : ' value="'.$timezone.'" ';
}

/* return string
 * Checks if $_GET month is set and selects appropriate option
 */
function monthPickerOption($month){
	global $my_month;
	return ($my_month == $month) ? ' selected ' : '';
}

/* return string
 * Checks if $_GET year is set and selects appropriate option
 */
function yearPickerOption($year){
	global $my_year;
	return ($my_year == $year) ? ' selected ' : '';
}

/* return integer
 * returns number of days in month based on offset (current month is offset 0)
 */
function getDaysInMonth($offset){
	global $my_month, $my_year;
	return cal_days_in_month(CAL_GREGORIAN, $my_month-0+$offset, $my_year);
}

/* return string
 * Based on $_GET variables, returns string for month and year
 */
function getCalendarTitle(){
	global $my_month, $my_year;
	$month_name = date('F', mktime(0, 0, 0, $my_month)); // March
	return $month_name . ' ' . $my_year;
}

/* return integer
 * Returns date (1-31) based off counter and starting date
 */
function getDayOfMonth($i){
	global $my_month, $my_year;
	return date("d", mktime(1,1,1, $my_month-1, $i, $my_year));
}

/* return integer
 * Returns the date of the first block on calendar (Ex: 27)
 */
function getStartingDate(){
	global $my_month, $my_year;
	return (getDaysInMonth(-1) - date("w", mktime(1,1,1,$my_month, 1, $my_year)) + 1);
}

/* return boolean
 * Checks if $i indicates today
 */
function isToday($i){
	global $my_month, $my_year;
	return mktime(1, 1, 1) == mktime(1, 1, 1, $my_month-1, $i, $my_year);
}

/* return boolean
 * Checks if $i indicates today's month
 */
function isCurrMonth($i){
	global $my_month, $my_year;
	return date('n', mktime(1, 1, 1, $my_month-1, $i, $my_year)) == $my_month;
}

/* return boolean
 * Checks if $i indicates next month
 */
function isNextMonth($i){
	global $my_month, $my_year;
	return date('n', mktime(1, 1, 1, $my_month-1, $i, $my_year)) >= date('n', mktime(1, 1, 1, $my_month+1, date('j'), $my_year));
}


/* return html string
 * 
 */
function createCalendar(){
	global $my_month, $my_year;
	$return = '';
	
	$i = getStartingDate();
	
	while (!isNextMonth($i)){ // while the month is not next
		$day_of_week = 0; // 0-6 (Sun-Sat)
		$return .= '<tr>';
		while ($day_of_week <= 6){
			
			$classes = array('cal-day');
			$classes[] = isToday($i) ? 'active-day' : '';
			$classes[] = isCurrMonth($i) ? 'curr-month' : '';

			$return .= '<td><div class=" ' . implode(' ', $classes) . '"><span class="cal-date">' . getDayOfMonth($i) . '</span></div></td>';
			
			$i++;
			$day_of_week++;
		}
		$return .= '</tr>';
	}
		
	return $return;
}