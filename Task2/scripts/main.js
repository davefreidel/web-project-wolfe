;(function($, window, document, undefined){
	
	var $mainForm,
		$nameField,
		$favoriteColorField,
		$favoriteColorPickerField,
		$favoritePetField,
		$birthdayField,
		$emailField,
		$phoneNumberField,
		$colorPickers,
		$submissionsTableBody;

	function init(){
		$mainForm = $("body form#main-form");
		$nameField = $("body form#main-form #name-field");
		$favoriteColorField = $("body form#main-form #favorite-color-field");
		$favoriteColorPickerField = $("body form#main-form #favorite-color-picker-field");
		$favoritePetField = $("body form#main-form #favorite-pet-field");
		$birthdayField = $("body form#main-form #birthday-field");
		$emailField = $("body form#main-form #email-field");
		$phoneNumberField = $("body form#main-form #phone-number-field");
		$colorPickers = $mainForm.find('.color-picker');
		$submissionsTableBody = $("body #submissions table tbody");
	}
	
	function initMainFormValidation(){
		$mainForm.find('input').on("blur keyup", function(evt){
			var $this = $(this);

			var isValid = validate($this);
			
			if (evt.type == 'keyup' && isValid){
				$this.attr("data-valid", isValid);
			}
			if (evt.type == 'blur'){
				$this.attr("data-valid", isValid);
			}
		});
	}
	
	function validate($input){
		var val = $input.val();
		var pattern = $input.data('pattern');
		var mods = $input.data('pattern-mods');
		var re = new RegExp(pattern, mods);
		var isValid = re.test(val);
		return isValid;
	}
	
	function initMainFormSubmission(){
		$mainForm.on("submit", function(){
			
			//Check if any fields are invalid
			var pass = true;
			$mainForm.find('input').each(function(){
				var $this = $(this);
				var isValid = validate($this);
				$this.attr("data-valid", isValid);
				if (!isValid){
					pass = false;
				}
			});
			
			if (pass){
				
				// Append a row to table
				var $tr = $("<tr></tr>");
				$tr.append("<td>" + $nameField.val() + "</td>");
				$tr.append("<td>" + $favoriteColorField.val() + "</td>");
				$tr.append("<td>" + $favoritePetField.val() + "</td>");
				$tr.append("<td>" + $birthdayField.val() + "</td>");
				$tr.append("<td>" + $emailField.val() + "</td>");
				$tr.append("<td>" + $phoneNumberField.val() + "</td>");
				$submissionsTableBody.append($tr);
				
				$nameField.val('').attr('data-valid', null);
				$favoriteColorPickerField.val('').attr('data-valid', null);
				$favoriteColorField.val('').attr('data-valid', null);
				$favoritePetField.val('').attr('data-valid', null);
				$birthdayField.val('').attr('data-valid', null);
				$emailField.val('').attr('data-valid', null);
				$phoneNumberField.val('').attr('data-valid', null);
			}else{
				alert("There are outstanding validation errors. Please correct and try again");
			}
			
			return false;
		});
	}
	
	function initColorPicker(){
		// Use a color picker and text input to create a nice fallback experience
		$colorPickers.each(function(){
			var $this = $(this);
			var $picker = $this.children('input[type=color]');
			var $display = $this.children('input[type=text]');
			
			$picker.on("change", function(){
				$display.val($picker.val());
				$display.focus();
			});
			$display.on("change keyup blur", function(evt){
				$picker.val($display.val());
				if (evt.type == 'change' || evt.type == 'keyup'){
					$display.focus();
				}
			});
		});
	}
	
	function initSortable(){
		$("body [data-sort]").on("click", function(){
			var $this = $(this);
			var $parent = $this.parent('th');
			var index = $parent.index();
			var sorted = true;
			var needToSort = false;
		
			// Loop through rows until no sorting is necessary
			while(sorted){
				var $rows = $submissionsTableBody.children('tr');
				sorted = false;
				
				for (i = 0; i < ($rows.length - 1); i++) {
					
					var $row = $rows.eq(i);
					var $next = $rows.eq(i-0+1);
					
					var $comp_1 = $row.children('td:eq('+index+')');
					var $comp_2 = $next.children('td:eq('+index+')');
					
					// "a" < "b"
					if ($comp_1.html().toLowerCase() > $comp_2.html().toLowerCase()){
						needToSort = true;
						break;
					}
				}
				
				if (needToSort){
					$row.insertAfter($next);
					sorted = true;
					needToSort = false;
				}
			}
		});
	}
	
	$(function(){
		init();
		initColorPicker();
		initMainFormValidation();
		initMainFormSubmission();
		initSortable();
	});
	
	
})(jQuery, window, document);