# README #

## Task 1 ##

1. I added a timezone offset selector. It really only has an effect at 3 am, but could be useful if checking London.
2. I added month and year drop downs.
3. Responsive feature shows only current month in agenda view.
4. The logic determines the first block's date (usually the end of last month) and increments by week using the date function. It lets PHP do the heavy lifting for determining the date. For instance, PHP converts June 34 to July 4.

## Task 2 ##

1. Sortable bonus feature added. Currently sorts in one direction.
2. Validation always annoys me when the field tells you the input is wrong before you even finish typing. So the error (red outline) only shows on blur, but once the input is correct, it shows correctness (green outline) on keyup.
3. I added a few default entries to be able to test sortable without having to enter a few submissions.
4. Color picker is a combination of two inputs that stay in sync. This is because some default implementations of the HTML5 color picker in browsers may not be desirable yet, so I added a text input field as well.

## Task 3 ##

1. I was not able to finish this task, but I think the items I added document a few elements that show my process and structure.
2. I used Bootstrap (mostly grid)